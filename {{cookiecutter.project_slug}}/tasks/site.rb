namespace :site do
  def make_index
    jsonfiles = Dir.glob("#{REPORTDIR}/**/*.json")
    htmlfiles = Dir.glob("#{REPORTDIR}/**/*.html")

    index = (jsonfiles + htmlfiles).map do |f|
      {
        'type' => File.extname(f)[1..-1],
        'filename' => f.gsub("#{REPORTDIR}/", '')
      }
    end
    File.write("#{REPORTDIR}/index.json", JSON.pretty_generate(index))
  end

  desc 'create dev site'
  task :build do
    make_index
  end
end
