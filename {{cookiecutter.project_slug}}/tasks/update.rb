desc 'update gem'
task update: :build do
  Dir.chdir(GEMDIR) do
    gem_name = "#{NAME}-#{VERSION}.gem"
    system("gem yank #{NAME} -v #{VERSION}")
    system("gem push #{gem_name}")
  end
end
