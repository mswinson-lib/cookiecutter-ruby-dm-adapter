require 'fileutils'

desc 'clean'
task :clean do
  FileUtils.rm_rf('target/')
  FileUtils.rm_rf('coverage/')
end
