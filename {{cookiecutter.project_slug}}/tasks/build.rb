require 'fileutils'

desc 'build gem'
task :build do
  FileUtils.mkdir_p(GEMDIR)
  system("gem build #{NAME}.gemspec")
  FileUtils.mv("#{NAME}-#{VERSION}.gem", GEMDIR)
end
