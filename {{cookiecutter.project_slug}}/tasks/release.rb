desc 'release gems'
task release: :build do
  Dir.chdir(GEMDIR) do
    gem_name = "#{NAME}-#{VERSION}.gem"
    system("gem push #{gem_name}")
  end
end
