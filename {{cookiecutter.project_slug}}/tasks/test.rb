# rubocop:disable Style/HashSyntax

require 'rspec/core/rake_task'
require 'json'

COVERAGESRC = './coverage'.freeze
COVERAGEDEST = "#{REPORTDIR}/coverage".freeze
RSPECDEST = "#{REPORTDIR}/rspec/".freeze

namespace :test do
  def move_reports(src, dest)
    return unless Dir.exist?(src)
    FileUtils.rm_rf(dest) if Dir.exist?(dest)
    FileUtils.mkdir_p(dest)
    FileUtils.mv(Dir.glob("#{src}/*"), dest)
    FileUtils.rm_rf(src)
  end

  RSpec::Core::RakeTask.new(:rspec_integration) do |t|
    t.pattern = './spec/integration/**/*_spec.rb'
    t.verbose = false
    t.rspec_opts = [
      '--format', 'html',
      '--out', "#{RSPECDEST}/rspec.html",
      '--format', 'json',
      '--out', "#{RSPECDEST}/rspec.json",
      '--format', 'documentation',
      '--color'
    ].join(' ')
  end

  RSpec::Core::RakeTask.new(:rspec_unit) do |t|
    t.pattern = './spec/unit/**/*_spec.rb'
    t.verbose = false
    t.rspec_opts = [
      '--format', 'html',
      '--out', "#{RSPECDEST}/rspec.html",
      '--format', 'json',
      '--out', "#{RSPECDEST}/rspec.json",
      '--format', 'documentation',
      '--color'
    ].join(' ')
  end

  desc 'run unit tests'
  task :unit do
    begin
      Rake::Task['test:rspec_unit'].invoke
    ensure
      move_reports(COVERAGESRC, "#{COVERAGEDEST}/unit")
    end
  end

  desc 'run integration tests'
  task :integration do
    begin
      Rake::Task['test:rspec_integration'].invoke
    ensure
      move_reports(COVERAGESRC, "#{COVERAGEDEST}/integration")
    end
  end

  desc 'run all tests'
  task :all => [:unit, :integration]

  Rake::Task['test:rspec_integration'].clear_comments
  Rake::Task['test:rspec_unit'].clear_comments
end
