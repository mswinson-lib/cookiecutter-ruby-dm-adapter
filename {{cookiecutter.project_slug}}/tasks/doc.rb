require 'yard'
require 'yardstick/rake/measurement'
require 'yardstick/rake/verify'

namespace :doc do
  YARD::Rake::YardocTask.new do |t|
    t.name = :build
    t.files = ['lib/**/*.rb']
    t.options = ['-o', APIDIR]
  end

  Yardstick::Rake::Measurement.new(:measure) do |measurement|
    measurement.output = "#{TARGETDIR}/errors-doc.err"
  end

  Yardstick::Rake::Verify.new(:verify) do |verify|
    verify.threshold = 100
  end

  task verify: :measure
end
