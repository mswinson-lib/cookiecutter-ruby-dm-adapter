require 'spec_helper'

describe('given a datamapper repository') do
  describe('{{cookiecutter.project_slug}}://') do
    before(:each) do
      DataMapper.setup(:default, '{{cookiecutter.project_slug}}://')
      DataMapper.finalize

      VCR.insert_cassette('{{cookiecutter.project_slug}}')
    end

    after(:each) do
      VCR.eject_cassette('{{cookiecutter.project_slug}}')
    end

    describe(DataMapper::Models::Rss::Item, '.all') do
      let(:model) do
        DataMapper::Models::Rss::Item
      end

      it('returns a collection of rss items') do
        results = model.all
        expect(!results.empty?).to be true
        expect(results.first).to be_instance_of(model)
      end
    end
  end
end
