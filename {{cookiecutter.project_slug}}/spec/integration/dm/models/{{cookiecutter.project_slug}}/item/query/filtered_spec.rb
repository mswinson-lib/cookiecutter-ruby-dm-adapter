# rubocop:disable Metrics/BlockLength
require 'spec_helper'

describe('given a datamapper repository') do
  describe('rss://feeds.feedburner.com/se-radio') do
    before(:each) do
      DataMapper.setup(:default, 'rss://feeds.feedburner.com/se-radio')
      DataMapper.finalize

      VCR.insert_cassette('rss')
    end

    after(:each) do
      VCR.eject_cassette('rss')
    end

    describe(DataMapper::Models::Rss::Item, '.all') do
      let(:model) do
        DataMapper::Models::Rss::Item
      end

      describe('items') do
        describe('with description containing " Ruby "') do
          it('returns a collection of Ruby items') do
            results = model.all(conditions: { :description.like => '% Ruby %' })

            expect(!results.empty?).to be true
            results.each do |item|
              expect(item.description =~ /.+Ruby.+/).not_to be_nil
            end
          end
        end
      end
    end
  end
end
