# rubocop:disable Metrics/BlockLength
require 'spec_helper'

describe('given a datamapper repository') do
  describe('rss://feeds.feedburner.com/se-radio') do
    before(:each) do
      DataMapper.setup(:default, 'rss://feeds.feedburner.com/se-radio')
      DataMapper.finalize

      VCR.insert_cassette('rss')
    end

    after(:each) do
      VCR.eject_cassette('rss')
    end

    describe(DataMapper::Models::Rss::Item, '.all') do
      describe('ordered by date , descending') do
        it('returns a sorted collection of rss items') do
          model = DataMapper::Models::Rss::Item

          results = model.all(order: [:pubDate.desc])

          expect(!results.empty?).to be true
          results.each_cons(2) do |first, second|
            expect(first.pubDate > second.pubDate).to be true
          end
        end
      end

      describe('ordered by date , ascending') do
        it('returns a sorted collection of rss items') do
          model = DataMapper::Models::Rss::Item

          results = model.all(order: [:pubDate.asc])

          expect(!results.empty?).to be true
          results.each_cons(2) do |first, second|
            expect(first.pubDate < second.pubDate).to be true
          end
        end
      end
    end
  end
end
