require 'spec_helper'

describe('given a datamapper repository') do
  describe('rss://feeds.feedburner.com/se-radio') do
    before(:each) do
      DataMapper.setup(:default, 'rss://feeds.feedburner.com/se-radio')
      DataMapper.finalize

      VCR.insert_cassette('rss')
    end

    after(:each) do
      VCR.eject_cassette('rss')
    end

    describe(DataMapper::Models::Rss::Item, '.all') do
      describe('limited to 20 records') do
        it('returns a collection of 20 rss items') do
          model = DataMapper::Models::Rss::Item

          results = model.all(limit: 20)

          expect(!results.empty?).to be true
          expect(results.length).to eql(20)
        end
      end
    end
  end
end
