require 'spec_helper'

describe('given an invalid rss repository') do
  before(:each) do
    DataMapper.setup(:default, 'rss://abcdef/efg')
    DataMapper.finalize

    VCR.insert_cassette('rss_fail')
  end

  after(:each) do
    VCR.eject_cassette('rss_fail')
  end

  describe(DataMapper::Models::Rss::Item, '.all') do
    subject { described_class }

    it('fails with a connection error') do
      expect do
        subject.first
      end.to raise_error(DataMapper::Adapters::RssAdapter::ConnectionError)
    end
  end
end
