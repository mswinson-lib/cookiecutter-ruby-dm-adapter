# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require '{{cookiecutter.project_slug}}/version'

Gem::Specification.new do |spec|
  spec.name          = '{{cookiecutter.project_slug}}'
  spec.version       = DataMapper::{{cookiecutter.resource_klass}}Adapter::VERSION
  spec.authors       = ['{{cookiecutter.author_name}}']
  spec.email         = ['{{cookiecutter.author_email}}']
  spec.summary       = '{{cookiecutter.project_summary}}'
  spec.homepage      = '{{cookiecutter.project_home}}'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_runtime_dependency 'dm-core'

  spec.add_development_dependency 'rake', '10.1.1'
  spec.add_development_dependency 'yard', '~> 0.8'
  spec.add_development_dependency 'yardstick', '~> 0.9'
  spec.add_development_dependency 'rspec', '~> 3.1'
  spec.add_development_dependency 'simplecov', '~> 0.9.0'
  spec.add_development_dependency 'simplecov-json', '~> 0.2'
  spec.add_development_dependency 'rubocop', '~> 0.28'
  spec.add_development_dependency 'rubocop-rspec', '~> 1.2'
end
