module DataMapper
  module Adapters
    class {{cookiecutter.resource_klass}}Adapter < AbstractAdapter
      def create(resources)
      end

      def read(query)
        query.filter_records(records_for(query.model).dup)
      end

      def update(attributes, collection)
      end

      def delete(collection)
      end

      private

      # fetch records
      # @api private
      # @param model [DataMapper::Resource] model
      # @return [Array]
      def records_for(model)
        []
      end

      # initialize adapter
      # @api private
      # @param name [String] adapter name
      # @param options [Hash] adapter options
      def initialize(name, options = {})
        super
      end
    end

    const_added(:{{cookiecutter.resource_klass}}Adapter)
  end
end
